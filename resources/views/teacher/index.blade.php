@extends('layouts.app')

@section('content')
<div class="grid grid-cols-1 place-items-center mt-12 mb-4">
    <h1 class="text-gray-700 text-3xl font-bold text-center mb-8">Listado de profesores</h1>
    <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        Nombre
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Apellido
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Estado
                    </th>   
                    <th scope="col" class="px-6 py-3">
                        Acción
                    </th>            
                </tr>
            </thead>
            <tbody>                
                @foreach ($teachers as $teacher)
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                    <th class="px-6 py-4">
                        {{ $teacher->user->name }}
                    </th>
                    <td class="px-6 py-4">
                        {{ $teacher->user->surname }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $teacher->state }}
                    </td> 
                    <td class="px-6 py-4 font-medium">
                        <a href="#">Dar alta</a>
                    </td>                                    
                </tr> 
                @endforeach            
            </tbody>
        </table>
    </div>
</div>
@endsection