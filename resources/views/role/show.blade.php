@extends('layouts.app')

@section('content')
<div class="grid grid-cols-1 place-items-center mt-12 mb-4">
    <h1 class="text-gray-700 text-3xl font-bold text-center mb-8">Rol de pago individual</h1>
    <div class="grid grid-cols-2 gap-4">
        <div class="col-span-2">
            <label for="" class="block mb-2 text-sm font-medium text-gray-800">Periodo:</label> {{ $role->period->description }}
        </div>
    
        <div>
            <label for="" class="block mb-2 text-sm font-medium text-gray-800">Empleado:</label> {{ $role->employee->name }} {{ $role->employee->surname }}
        </div>
    
        <div>
            <label for="" class="block mb-2 text-sm font-medium text-gray-800">Cargo:</label> {{ $role->employee->name }} {{ $role->employee->position }}
        </div>

        <hr class="col-span-2">

        <div>
            <label for="" class="block mb-2 text-sm font-medium text-gray-800">Ingresos:</label> 
            @foreach ($role->details as $detail)
                @if ($detail->concept == 'ingreso')
                    <div>{{ $detail->description }} = ${{ $detail->value }}</div>                    
                @endif
            @endforeach
        </div>

        <div>
            <label for="" class="block mb-2 text-sm font-medium text-gray-800">Egresos:</label> 
            @foreach ($role->details as $detail)
                @if ($detail->concept == 'egreso')
                <div>{{ $detail->description }} = ${{ $detail->value }}</div>
                @endif
            @endforeach
        </div>

        <hr class="col-span-2">

        <div>
            <label for="" class="block mb-2 text-sm font-medium text-gray-800">Total ingresos:</label> {{ $role->total_income }}
        </div>

        <div>
            <label for="" class="block mb-2 text-sm font-medium text-gray-800">Total egresos:</label> {{ $role->total_expense }}
        </div>

        <div class="col-span-2">
            <label for="" class="block mb-2 text-sm font-medium text-gray-800">Neto a pagar:</label> {{ $role->value_to_pay }}
        </div>

        <hr class="col-span-2">
        
        <a href="{{ route('create-pdf', $role) }}" class="text-white bg-gray-800 hover:bg-gray-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center mt-5 col-span-2">Exportar pdf</a>

    </div>
</div>
@endsection