<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="grid grid-cols-1 place-items-center mt-12 mb-4">
        <h1 class="text-gray-700 text-3xl font-bold text-center mb-8">Rol de pago individual</h1>
        <div class="grid grid-cols-2 gap-4">
            <div class="col-span-2">
                <label for="" class="block mb-2 text-sm font-medium text-gray-800">Periodo:</label> {{ $data['role']->period->description }}
            </div>
        
            <div>
                <label for="" class="block mb-2 text-sm font-medium text-gray-800">Empleado:</label> {{ $data['role']->employee->name }} {{ $role->employee->surname }}
            </div>
        
            <div>
                <label for="" class="block mb-2 text-sm font-medium text-gray-800">Cargo:</label> {{ $data['role']->employee->name }} {{ $role->employee->position }}
            </div>
    
            <hr class="col-span-2">
    
            <div>
                <label for="" class="block mb-2 text-sm font-medium text-gray-800">Ingresos:</label> 
                @foreach ($data['role']->details as $detail)
                    @if ($detail->concept == 'ingreso')
                        <div>{{ $detail->description }} = ${{ $detail->value }}</div>                    
                    @endif
                @endforeach
            </div>
    
            <div>
                <label for="" class="block mb-2 text-sm font-medium text-gray-800">Egresos:</label> 
                @foreach ($data['role']->details as $detail)
                    @if ($detail->concept == 'egreso')
                    <div>{{ $detail->description }} = ${{ $detail->value }}</div>
                    @endif
                @endforeach
            </div>
    
            <hr class="col-span-2">
    
            <div>
                <label for="" class="block mb-2 text-sm font-medium text-gray-800">Total ingresos:</label> {{ $data['role']->total_income }}
            </div>
    
            <div>
                <label for="" class="block mb-2 text-sm font-medium text-gray-800">Total egresos:</label> {{ $data['role']->total_expense }}
            </div>
    
            <div class="col-span-2">
                <label for="" class="block mb-2 text-sm font-medium text-gray-800">Neto a pagar:</label> {{ $data['role']->value_to_pay }}
            </div>
    
        </div>
    </div>
</body>
</html>