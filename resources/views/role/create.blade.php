@extends('layouts.app')

@section('content')

<div class="grid grid-cols-1 place-items-center mt-12 mb-4">
    <h1 class="text-gray-700 text-3xl font-bold text-center mb-8">Generar Rol de Pago</h1>
    <form action="{{ route('save-role', $employee) }}" method="post" class="md:w-1/3 w-full pr-12 pl-12 grid grid-cols-2 gap-4">
        @csrf
        <div>
            <label for="" class="block mb-2 text-sm font-medium text-gray-800">Nombre:
                <input type="text" value="{{ $employee->name }}" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" disabled>
            </label>
        </div>

        <div>
            <label for="" class="block mb-2 text-sm font-medium text-gray-800">Apellido:
                <input type="text" value="{{ $employee->surname }}" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" disabled>
            </label>       
        </div>
        
        <div>
            <label for="" class="block mb-2 text-sm font-medium text-gray-800">Cargo:
                <input type="text" value="{{ $employee->position }}" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" disabled>
            </label>
        </div>
        
        <div>
            <label for="" class="block mb-2 text-sm font-medium text-gray-800">Sueldo:
                <input type="text" value="{{ $employee->salary }}" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" disabled>
            </label>            
        </div>  
        
        <hr class="col-span-2">

        <div class="col-span-2">
            <label for="" class="block mb-2 text-sm font-medium text-gray-800">Periodo:</label>
            <select name="period" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required="">
                @foreach ($periods as $period)
                    <option value="{{$period->id}}">{{$period->description}}</option>
                @endforeach                          
            </select>  
        </div>

        <hr class="col-span-2">

        <button id="button_add_detail" type="button" class="text-white bg-gray-800 hover:bg-gray-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center mt-5">Agregar detalle</button>
        
        <div class="field_wrapper col-span-2 grid grid-cols-2 gap-4"></div>

        <hr class="col-span-2">
        
        <button type="submit" class="text-white bg-gray-800 hover:bg-gray-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center mt-5 col-span-2">Generar</button>
    </form>
</div>

<script type="text/javascript">
   
    $( document ).ready(function() {
        var wrapper = $('.field_wrapper');
        var key = 0;         

        $( "#button_add_detail" ).click(function() {
            var fieldHTML = '<div>' +
                            '<label for="" class="block text-sm font-medium text-gray-800">Concepto:</label>' +
                            '<select name="details['+ key +'][concept]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required="">' +
                                '<option value="ingreso">ingreso</option>' +
                                '<option value="egreso">egreso</option>' +
                            '</select>' +
                        '</div>' +

                        '<label for="" class="block mb-2 text-sm font-medium text-gray-800">Valor:' +
                            '<input type="number" name="details['+ key +'][value]" id="" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required="">' +
                        '</label>' +

                        '<label for="" class="block mb-2 text-sm font-medium text-gray-800 col-span-2">Descripción (ej: préstamo, horas extras):' +
                            '<input type="text" name="details['+ key +'][description]" id="" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required="">' +
                        '</label>';
            $(wrapper).append(fieldHTML);    
            key++;
        });
        
    });

</script>

@endsection