@extends('layouts.app')

@section('content')
<div class="grid grid-cols-1 place-items-center mt-12 mb-4">
    <div class="p-4 mb-4 text-sm text-yellow-700 bg-yellow-100 rounded-lg dark:bg-yellow-200 dark:text-yellow-800" role="alert">
        <span class="font-medium">Alerta!</span> {{ $message }}
    </div>
    <a href="{{ route('employees') }}">Volver al listado de emplados</a>
</div>
@endsection