@extends('layouts.app')

@section('content')
<div class="grid grid-cols-1 place-items-center mt-12 mb-4">
    <h1 class="text-gray-700 text-3xl font-bold text-center mb-8">Busqueda rol de pagos</h1>
    <form action="{{ route('search-roles') }}" method="get" class="md:w-1/3 w-full pr-12 pl-12 grid grid-cols-2 gap-4">
    
        <div>
            <label for="" class="block mb-2 text-sm font-medium text-gray-800">Periodo:</label>
            <select name="period" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required="">
                @foreach ($periods as $period)
                    <option value="{{$period->id}}">{{$period->description}}</option>
                @endforeach                          
            </select>  
        </div>
    
        <button type="submit" class="text-white bg-gray-800 hover:bg-gray-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center mt-5">Buscar</button>
    
    </form>

    @if (!empty($roles))
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-12">
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            Nombre
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Apellido
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Cargo
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Periodo
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Acciones
                        </th>
                    </tr>
                </thead>
                <tbody>                
                    @foreach ($roles as $role)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                        <th class="px-6 py-4">
                            {{ $role->employee->name }}
                        </th>
                        <td class="px-6 py-4">
                            {{ $role->employee->surname }}
                        </td>
                        <td class="px-6 py-4">
                            {{ $role->employee->position }}
                        </td>
                        <td class="px-6 py-4">
                            {{ $role->period->description }}
                        </td>                    
                        <td class="px-6 py-4 font-medium">
                            <a href="{{ route('show-role', $role) }}">Ver rol</a>
                        </td>                    
                    </tr> 
                    @endforeach            
                </tbody>
            </table>
        </div>
        <a href="{{ route('create-excel', $period_id) }}" class="text-white bg-gray-800 hover:bg-gray-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center mt-5">Exportar excel</a>
    @endif
</div>
@endsection