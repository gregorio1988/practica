@extends('layouts.app')

@section('content')

<div class="grid grid-cols-1 place-items-center mt-12 mb-4">
    <h1 class="text-gray-700 text-3xl font-bold text-center mb-8">Crear curso</h1>
    <form action="{{ route('courses.store') }}" method="post" class="md:w-1/3 w-full pr-12 pl-12">
        @csrf
        <label for="" class="block mb-2 text-sm font-medium text-gray-800">Nombre</label>
        <input type="text" name="name" id="" value="{{ old('name') }}" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">
        @error('name')
            <small class="text-red-700">{{ $message }}</small>
        @enderror        
        <label for="" class="block mb-2 text-sm font-medium text-gray-800">Estado</label>
        <select name="state" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required="">
            <option value="activo">activo</option>
            <option value="inactivo">inactivo</option>                        
        </select>  
        @error('state')
            <small class="text-red-700">{{ $message }}</small>
        @enderror      
        <input type="hidden" name="teacher_id" value="{{ $teacher_id }}">
        <button type="submit" class="text-white bg-gray-800 hover:bg-gray-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center mt-5">Grabar</button>
    </form>
</div>
@endsection