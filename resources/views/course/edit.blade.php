@extends('layouts.app')

@section('content')

<div class="grid grid-cols-1 place-items-center mt-12 mb-4">
    <h1 class="text-gray-700 text-3xl font-bold text-center mb-8">Editar curso</h1>
    <form action="{{ route('courses.update', $course) }}" method="post" class="md:w-1/3 w-full pr-12 pl-12">
        @csrf
        @method('put')
       
        <label for="" class="block mb-2 text-sm font-medium text-gray-800">Nombre</label>
        <input type="text" name="name" id="" value="{{ $course->name }}" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5">               
        
        <label for="" class="block mb-2 text-sm font-medium text-gray-800">Estado</label>
        <select name="state" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required="">
            <option value="activo" {{ $course->state == "activo" ? 'selected' : '' }}>activo</option>
            <option value="inactivo" {{ $course->state == "inactivo" ? 'selected' : '' }}>inactivo</option>                        
        </select> 
                
        <button type="submit" class="text-white bg-gray-800 hover:bg-gray-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center mt-5">Grabar</button>
    </form>
</div>
@endsection