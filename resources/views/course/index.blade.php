@extends('layouts.app')

@section('content')
<div class="grid grid-cols-1 place-items-center mt-12 mb-4">
    <h1 class="text-gray-700 text-3xl font-bold text-center mb-8">Listado de cursos</h1>
    <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        Nombre
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Estado
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Acciones
                    </th>                    
                </tr>
            </thead>
            <tbody>                
                @foreach ($courses as $course)
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                        {{ $course->name }}
                    </th>
                    <td class="px-6 py-4">
                        {{ $course->state }}
                    </td>                   
                    <td class="px-6 py-4">
                        <a href="{{ route('courses.edit', $course) }}">Editar</a>
                        <form action="{{ route('courses.destroy', $course) }}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit">Eliminar</button>
                        </form>
                    </td>                    
                </tr> 
                @endforeach            
            </tbody>
        </table>
    </div>
    <a href="{{ route('courses.create') }}" class="text-white bg-gray-800 hover:bg-gray-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center mt-5">Crear curso</a>
</div>
@endsection