@extends('layouts.app')

@section('content')

<div class="grid grid-cols-1 place-items-center mt-12 mb-4">
    <h1 class="text-gray-700 text-3xl font-bold text-center mb-8">Bienvenido {{ auth()->user()->name }}</h1>
</div>

@endsection
