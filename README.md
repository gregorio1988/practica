## Indicaciones para levantar el proyecto
Ejecutar en la raíz del proyecto: 

Copiamos el archivo de entorno
```
cp .env.example .env
```

Hacemos build y run del docker
```
docker-compose up --build -d
```
Ejecutamos los siguientes comandos: 
```
docker exec -it Laravel_php2 composer install 
```
Luego para generar el key ejecutar comando:
```
docker exec -it Laravel_php2 php artisan key:generate
```
Para descargar las dependencias del proyecto, ademas ejecutar:
```
docker exec -it Laravel_php2 composer dump-autoload
docker exec -it Laravel_php2 php artisan config:cache
docker exec -it Laravel_php2 php artisan route:clear
```
Accedemos al contenedor de postgres
```
docker exec -it Laravel_postgres2 /bin/bash
```
```
psql -U postgres
```
```
CREATE DATABASE prueba;
```

Salimos del contenedor

Ejecutamos las migraciones para crear las tablas
```
docker exec -it Laravel_php2 php artisan migrate:fresh --seed
```
Para ingresar al sistema  se debe utilizar las siguientes credenciales:
```
email: admin@admin.com
contraseña: 12345678
```

```
email: mariacastro@consulti.ec
contraseña: 123
```