<?php

namespace App\Exports;

use App\Models\Employee;
use App\Models\Role;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RolesExport implements FromCollection, WithHeadings
{
    protected $period_id;
    
    /**
     * 
     */
    public  function __construct($period_id) {
        $this->period_id = $period_id;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
            'nombre',
            'apellido',
            'periodo',
            'total ingreso',
            'total egreso',
            'neto a pagar',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $employee = Employee::where(['user_id' => auth()->user()->id])->first();
        $arraySearch = (auth()->user()->role == 'empleado') ? ['employee_id' => $employee->id] : [];
        $roles = Role::join('employees', 'employee_id', '=', 'employees.id')
                ->join('periods', 'period_id', '=', 'periods.id')
                ->where(array_merge(['period_id' => $this->period_id], $arraySearch))
                ->select('employees.name', 'employees.surname', 'periods.description', 'total_income', 'total_expense', 'value_to_pay')
                ->get();
        //$roles = Role::select('employee_id', 'period_id', 'total_income', 'total_expense', 'value_to_pay')->where(array_merge(['period_id' => $this->period_id], $arraySearch))->get();

        return $roles;
    }
}
