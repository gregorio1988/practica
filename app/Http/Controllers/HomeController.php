<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $role = $this->getRole();
        return view('home', compact('role'));
    }

    public function getRole()
    {
        $user = User::find((auth()->user()->id));
        
        if ($user->role == 'administrador') {
            $role = 'administrador';
        }elseif (is_null($user->teacher)) {
            $role = 'consumidor';
        }else{
            $role = 'creador';
        }

        return $role;
    }
}
