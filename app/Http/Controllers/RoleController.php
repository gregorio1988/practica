<?php

namespace App\Http\Controllers;

use App\Exports\RolesExport;
use App\Models\Employee;
use App\Models\Period;
use App\Models\Role;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="L5 OpenApi",
 *      description="L5 Swagger OpenApi description",
 *      x={
 *          "logo": {
 *              "url": "https://via.placeholder.com/190x90.png?text=L5-Swagger"
 *          }
 *      },
 *      @OA\Contact(
 *          email="gregorio.chisan@gmail.com"
 *      ),
 *      @OA\License(
 *         name="Apache 2.0",
 *         url="https://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 */
class RoleController extends Controller
{
    /**
     * Muestra una vista para crear un rol de pagos.
     *  @return \Illuminate\Contracts\Support\Renderable
     *
     * @OA\Get(
     *     path="generar-rol/{employee}",
     *     tags={"Pantalla creación rol de pagos"},
     *     summary="Mostrar creación de rol",
     *      @OA\Parameter(
     *         description="Parámetro necesario para creación de rol de pagos",
     *         in="path",
     *         name="employee",
     *         required=true,
     *         @OA\Schema(type="object")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar creación de rol de pagos."
     *     )
     * ) 
     */
    public function create(Employee $employee)
    {
        $periods = Period::all();
        return view('role.create', compact('employee', 'periods'));
    }

    /**
     *  Permite guardar un rol de pagos.
     *  @return \Illuminate\Contracts\Support\Renderable
     *
     * @OA\Get(
     *     path="guardar-rol/{employee}",
     *     tags={"Guardar rol de pagos"},
     *     summary="Permite guardar un rol de pagos",
     *      @OA\Parameter(
     *         description="Parámetro necesario para guardar un rol de pagos",
     *         in="path",
     *         name="employee",
     *         required=true,
     *         @OA\Schema(type="object")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Guarda un rol de pagos"
     *     )
     * ) 
     */
    public function store(Request $request, Employee $employee)
    {               
        $rol = $employee->roles()->where(['period_id' => $request->all()['period']])->first();

        if (is_null($rol)) {

            $discount_iess = Role::getDiscountIess($employee->salary);
            $details = isset($request->all()['details']) ? $request->all()['details'] : [];
            $totals = $this->getTotals($details);

            $role = Role::create([
                'employee_id'   => $employee->id,
                'period_id'     => $request->all()['period'],
                'total_income'  => $totals['total_income'] + $employee->salary,
                'total_expense' => $totals['total_expense'] + $discount_iess,
                'value_to_pay'  => ($totals['total_income'] + $employee->salary) - ($totals['total_expense'] + $discount_iess)
            ]);

            $this->createDetails($role, [
                'concept'     => 'egreso',
                'value'       => $discount_iess,
                'description' => 'descuento iess',
            ]);

            $this->createDetails($role, [
                'concept'     => 'ingreso',
                'value'       => $employee->salary,
                'description' => 'salario',
            ]);
            
            if (!empty($details)) {
                foreach($request->all()['details'] as $detail){
                    $this->createDetails($role, $detail);
                }
            }                        

            return redirect()->route('employees');

        }else{
            $message = "Rol ya existe para ese período";
            return view('role.error', compact('message'));
        }
            
    }

    /**
     * Get totals for save payment rol
     * 
     * @param array $details
     */
    public function getTotals(array $details)
    {
        $total_income = 0;
        $total_expense = 0;

        collect($details)->each(function ($detail) use (&$total_income, &$total_expense) {
            if ($detail['concept'] == 'ingreso') {
                $total_income += $detail['value'];
            }else{
                $total_expense += $detail['value'];
            }
        });

        return [
            'total_income'  => $total_income,
            'total_expense' => $total_expense
        ];
    }

    /**
     * Create detail payment rol
     * 
     * @param Rol $role
     * @param $detail
     */
    public function createDetails(Role $role, $detail)
    {
        return $role->details()->create($detail);
    }

    /**
     * Show view for search roles
     */
    public function searchRoles(Request $request)
    {
        $periods = Period::all();
        $employee = Employee::where(['user_id' => auth()->user()->id])->first();
        $arraySearch = (auth()->user()->role == 'empleado') ? ['employee_id' => $employee->id] : [];
        $roles = isset($request->all()['period']) ? Role::where(array_merge(['period_id' => $request->all()['period']], $arraySearch))->get() : [];
        $period_id = isset($request->all()['period']) ? $request->all()['period'] : '';
        
        return view('role.search_role', compact('periods', 'roles', 'period_id'));
    }

    /**
     * Show view for payment rol individual
     */
    public function show(Role $role)
    {
        return view('role.show', compact('role'));
    }

    /**
     * Create pdf for payment role
     */
    public function createPdf(Role $role)
    {
        $data = ['role' => $role];
        view()->share('data', $data);
        $pdf = PDF::loadView('role.create-pdf', $data);

        return $pdf->download('prueba.pdf');
    }

    /**
     * Create excel for payment roles
     */
    public function createExcel($period_id){
        return Excel::download(new RolesExport($period_id), 'roles.xlsx');
    }
}
