<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use App\Models\User;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    public function index()
    {
        $role = $this->getRole();
        $teachers = Teacher::all();
        return view('teacher.index', compact('teachers', 'role'));
    }

    public function getRole()
    {
        $user = User::find((auth()->user()->id));

        if ($user->role == 'administrador') {
            $role = 'administrador';
        }elseif (is_null($user->teacher)) {
            $role = 'consumidor';
        }else{
            $role = 'creador';
        }

        return $role;
    }
}
