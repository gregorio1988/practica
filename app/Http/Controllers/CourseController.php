<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index(){
        $role = $this->getRole();
        $teacher = Teacher::where(['user_id' => auth()->user()->id])->first();
        $courses = Course::where(['teacher_id' => $teacher->id])->get();
        return view('course.index', compact('courses', 'role'));
    }

    /**
     * Create course
     */
    public function create()
    {
        $role = $this->getRole();
        $teacher_id = $teacher = Teacher::where(['user_id' => auth()->user()->id])->first()->id;
        return view('course.create', compact('teacher_id', 'role'));
    }

    /**
     * Save course
     */
    public function store(Request $request)
    {
        $course = Course::create($request->all());
        $course->teacher_id = $request->all()['teacher_id'];
        $course->save();
        return redirect()->route('courses.index', $course);
    }

    /**
     * Show view edit course
     */
    public function edit(Course $course)
    {
        $role = $this->getRole();
        return view('course.edit', compact('course', 'role'));
    }

    /**
     * Edit course
     */
    public function update(Request $request, Course $course)
    {
        $course->name = $request->name;
        $course->state = $request->state;
        $course->save();
        
        return redirect()->route('courses.index', $course);
    }

    /**
     * Delete course
     */
    public function destroy(Course $course)
    {
        $course->delete();

        return redirect()->route('courses.index');
    }

    /**
     * Get role of user
     */
    public function getRole()
    {
        $user = User::find((auth()->user()->id));

        if ($user->role == 'administrador') {
            $role = 'administrador';
        }elseif (is_null($user->teacher)) {
            $role = 'consumidor';
        }else{
            $role = 'creador';
        }

        return $role;
    }
}
