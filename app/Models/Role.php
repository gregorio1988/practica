<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    const DISCOUNT_IESS = 9.35;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id',
        'period_id',
        'total_income',
        'total_expense',
        'value_to_pay'
    ];

    protected $primaryKey = 'id';

    /**
     * Relation one to many (inverse) with Employee
     */
    public function employee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id');
    }

    /**
     * Relation many to many with Detail
     */
    public function details()
    {
        return $this->belongsToMany('App\Models\Detail');
    }

    /**
     * Relation one to many (inverse) with Period
     */
    public function period()
    {
        return $this->belongsTo('App\Models\Period');
    }

    /**
     * Get value discount Iess
     */
    static public function getDiscountIess($salary)
    {
        return round($salary / self::DISCOUNT_IESS, 2);
    }
}
