<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'concept',
        'description',
        'value'
    ];


    /**
     * Relation many to many with Role
     */
    public function roles()
    {
        return $this->belongsToMany('App\Models\Role');
    }
}
