<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    use HasFactory;

    /**
     * Relation one to many with Role
     */
    public function roles()
    {
        return $this->hasMany('App\Models\Role');
    }
}
