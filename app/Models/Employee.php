<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    /**
     * Relation one to many with Role
     */
    public function roles()
    {
        return $this->hasMany('App\Models\Role', 'employee_id');
    }

    //Relation one to one(inverse) with User
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
