<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'state',
    ];

    /**
     * Relation one to many (inverse) with Teacher
     */
    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher');
    }

     /**
     * Relation many to many with Student
     */
    public function students()
    {
        return $this->belongsToMany('App\Models\Student');
    }
}
