<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state',
    ];

    //Relation one to one(inverse) with User
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Relation one to many with Course
     */
    public function courses()
    {
        return $this->hasMany('App\Models\Courses');
    }
}
