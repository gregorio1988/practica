<?php

namespace Database\Seeders;

use App\Models\Teacher;
use Illuminate\Database\Seeder;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teacher = new Teacher();
        $teacher->state = "activo";
        $teacher->user_id = "2";
        $teacher->save();

        $teacher2 = new Teacher();
        $teacher2->state = "inactivo";
        $teacher2->user_id = "3";
        $teacher2->save();
    }
}
