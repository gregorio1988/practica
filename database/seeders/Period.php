<?php

namespace Database\Seeders;

use App\Models\Period as ModelPeriod;
use Illuminate\Database\Seeder;

class Period extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $period = new ModelPeriod();
        $period->description = 'marzo-2022';
        $period->save();

        $period2 = new ModelPeriod();
        $period2->description = 'abril-2022';
        $period2->save();

        $period3 = new ModelPeriod();
        $period3->description = 'mayo-2022';
        $period3->save();
    }
}
