<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = "Gregorio";
        $user->surname = "Chiriguaya";
        $user->email = "admin@admin.com";
        $user->password = Hash::make('12345678');
        $user->role = "administrador";
        $user->save();

        $user1 = new User();
        $user1->name = "Maria";
        $user1->surname = "Castro";
        $user1->email = "mariacastro@consulti.ec";
        $user1->password = Hash::make('123');
        $user1->role = "usuario";
        $user1->save();

        $user2 = new User();
        $user2->name = "Enrique";
        $user2->surname = "Pozo";
        $user2->email = "enriquepozo@consulti.ec";
        $user2->password = Hash::make('123');
        $user2->role = "usuario";
        $user2->save();

        $user3 = new User();
        $user3->name = "Martha";
        $user3->surname = "Sanchez";
        $user3->email = "marthasanchez@consulti.ec";
        $user3->password = Hash::make('123');
        $user3->role = "usuario";
        $user3->save();

        $user4 = new User();
        $user4->name = "Carolina";
        $user4->surname = "Peralta";
        $user4->email = "carolinaperalta@consulti.ec";
        $user4->password = Hash::make('123');
        $user4->role = "usuario";
        $user4->save();
    }
}
