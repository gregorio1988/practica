<?php

namespace Database\Seeders;

use App\Models\Employee as ModelsEmployee;
use Illuminate\Database\Seeder;

class Employee extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employee = new ModelsEmployee();
        $employee->name = "Gregorio";
        $employee->surname = "Chiriguaya";
        $employee->position = "gerente de proyecto";
        $employee->salary = 1300;
        $employee->user_id = 1;
        $employee->save();

        $employee2 = new ModelsEmployee();
        $employee2->name = "Emilys";
        $employee2->surname = "Beltran";
        $employee2->position = "programador junior";
        $employee2->salary = 700;
        $employee2->user_id = 2;
        $employee2->save();

        $employee3 = new ModelsEmployee();
        $employee3->name = "Juan";
        $employee3->surname = "Velasco";
        $employee3->position = "gerente de proyecto";
        $employee3->salary = 1300;
        $employee3->user_id = 3;
        $employee3->save();

        $employee4 = new ModelsEmployee();
        $employee4->name = "Pedro";
        $employee4->surname = "Tomala";
        $employee4->position = "programador senior";
        $employee4->salary = 1000;
        $employee4->user_id = 4;
        $employee4->save();

        $employee5 = new ModelsEmployee();
        $employee5->name = "Cristian";
        $employee5->surname = "Martinez";
        $employee5->position = "programador senior";
        $employee5->salary = 1000;
        $employee5->user_id = 5;
        $employee5->save();
    }
}
