<?php

namespace Database\Seeders;

use App\Models\Student;
use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $student = new Student();
        $student->state = "activo";
        $student->user_id = "4";
        $student->save();

        $student2 = new Student();
        $student2->state = "activo";
        $student2->user_id = "5";
        $student2->save();
    }
}
