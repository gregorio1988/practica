<?php

namespace Database\Factories;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Employee::class;
    protected $salary = [
        'gerente de proyecto' => 1300,
        'programador junior' => 700,
        'programador senior' => 1000
    ];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $position = $this->faker->randomElement(['gerente de proyecto','programador junior','programador senior']);

        return [
            'name' => $this->faker->firstName(),
            'surname' => $this->faker->lastName(),
            'position' => $position,
            'salary' => $this->salary[$position]
        ];
    }

}
